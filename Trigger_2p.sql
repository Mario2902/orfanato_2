
create trigger TR_INSERT_HUERFANO
on HUERFANO for insert
as
declare @edad int
declare @grupo int
declare @detalle varchar (50)
select @edad= DATEDIFF(YEAR,HUERFANO_FECHA_NACIMIENTO,GETDATE())
			-(CASE WHEN DATEADD(YY,DATEDIFF(YEAR,HUERFANO_FECHA_NACIMIENTO,GETDATE()),HUERFANO_FECHA_NACIMIENTO)>GETDATE() 
			THEN 1 ELSE 0 END) 
			from inserted
if(@edad<15)
begin
	select @grupo=(
		CASE
			WHEN (@edad>=0 AND @edad<2) THEN 0
			WHEN (@edad>=2 AND @edad<6) THEN 1
			WHEN (@edad>=6 AND @edad<11) THEN 2
			ELSE 3
		END
	)
	select @detalle= (select CONCAT(GRUPO_DESCRIPCION, ' ', GRUPO_RANGO) from GRUPO where GRUPO_ID=@grupo)
	update HUERFANO SET GRUPO_ID=@grupo where HUERFANO_IDENT=(select HUERFANO_IDENT from inserted)
	raiserror (N'SE HA INGRESADO AL HUERFANO %d A�OS HA SIDO ASIGNADO AL GRUPO %s', 10, 1,@edad,@detalle)
end
else
begin
    raiserror ('EL INDIVIDUO ES MAYOR DE 15 A�OS, POR FAVOR ENVIAR A REFORMATORIO', 16, 1)
    rollback transaction
end;
